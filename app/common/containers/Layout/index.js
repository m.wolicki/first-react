import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';

import '@styles/main.scss';

const Layout = ({ children }) => (
  <div>
    <div className="page-wrapper">
      <Head>
        <title key="title">Example Title</title>
      </Head>
      <main>
        {children}
      </main>
    </div>
  </div>
);

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Layout;
